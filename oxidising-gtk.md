% Oxidising GTK
% Emmanuele Bassi
% GUADEC 2023

## Disclaimer

::: notes

As usual for my presentations, there's a disclaimer. A lot of this presentation is a thought experiment; as such, I don't want people to misconstrue this presentation as anything but my personal ideas and thoughts. The tone of this whole presentation may sound a bit dire; please, do not be alarmed. While I think things need to change in the future, and that ultimately there is a time limit, I don't think we are on a clock—yet. After all, it's been over 25 years, we can take some more time to get things right. This means I'm mainly trying to impress upon you not a feeling of urgency, but one of possibility. There is work to do, but there's space for experimentation.

:::

----

# Part I: Why?

----

## History is a harsh mistress

::: notes

GTK is old, no point dancing around the matter. It encodes over 25 years of knowledge on how to write a GUI toolkit for Linux, across different display servers and iterations of the graphics subsystems and rendering models. Depending on how you count it, GTK4 is between 7 and 3 years old, and with it we managed to shed a ton of cruft, but some of the internals are still the way they are because that's how they were introduced. Input, text, layout, even windowing system backends. GTK implements a complete toolkit for X11; a fairly complete toolkit for Wayland, minus some optional extensions; and a somewhat competent toolkit for Windows and macOS. We refactor what we can, and we drop broken abstractions and layering violations, but the length of each iteration has to be counterbalanced with the ability of application developers to sustain the rate of change. We spent nine years with the 2.x series, and nine years with the 3.x series; the 4.x series will likely be shorter, but with each iteration we need to provide larger and better incentives for porting applications and environments to a new major version—otherwise GTK will become a science experiment, perfect as it might be, used precisely by nobody.

:::

----

![](./images/gobject-meme.jpg)

::: notes

GObject is just only slightly newer than GTK: less than four years, give or take, which means it's about 20 years old, and was designed for a very different era. While GObject subsumed the original GTK type system and piled a bunch of functionality on top, the basics are still the same: a run time type system that implements bits of the C++ and Java object models, with multi-dispatch in the form of signals, and state mutators in the form of properties. Sadly, the same reason why GObject allows us to create a complete software development platform is also the reason why touching the type system is incredibly complicated. The run time aspect is problematic; it has very little compile time aspect, which means we need to run code in order to introspect its state, and it's why introspection cannot be cross-compiled without using binary wrappers and native toolchains. It's also the reason why we need to run complex test suites just to verify that people have been setting up properties correctly. The run time aspect also introduces an impedance mismatch between GObject and dynamic languages. The limitations of C and GObject bite us when it comes to the threading model, and impose complexity on a code base that becomes harder and harder to update and improve. There are very, very few people left who understand how the type system even works, these days.

:::

----

## Slowly renewable resources

::: notes

It's not all about technological issues; it actually rarely is. The core of our social issues is this: we're still using C, a language that is type safe if and only if all your types are integers—and even there, it's terrible at defining what those types actually are (see: `enum` and `bool` sized, or `char`'s signedness) or what the language does with them (see: integer overflow). The main problem is not undefined or implementation defined behaviours, though; the real issue is that C developers are a *very* slowly renewable resource. We can write documentation, improve tutorials, and mentor newcomers every day and twice on a Sunday, but the inescapable truth is that the world has, by and large, moved on. Great for us, who rely on C for job security, just like our COBOL ancestors before us; but it's bad for GTK, because at some point there's going to be very few people left capable of affecting change on it—or at all, considering the cost of changes on the larger ecosystem. At the end of the day, you may like C just as much as somebody else dislikes it, but if a project doesn't have new contributors we might as well pack everything up and leave.

:::

----

## Safety in numbers

::: notes

We have cycles in the development of GNOME applications. Once upon a time, we had people writing them in Python; then in Vala; then in JavaScript; and these days, it's all Rust. The reason why this happens is, in part, because of hype, which is what all of IT runs on; but, mostly, because of a network effect: people see new projects being written in a new language, and that gives them a certain "security blanket"; if they find an issue, they can look at other people's code, or ask other developers. The end result is that more people file issues, submit merge requests, write tutorials, and get involved. Library development in the GNOME community has been stuck on C for 25 years, and even getting people to contribute documentation, these days, has gotten really hard, and it has nothing to do with how we write documentation—we made that exceedingly easy. The problem is that you can only ask a few people about issues with C development, and they are already busy doing the job of a team 10 times the size.

:::

----

## High level languages

::: notes

Finally, let's admit it: C might allow you to do a lot of nasty, clever things, but it's really terrible at it every step of the way. Even introducing C99 into the mix, with designated initialisers, compound literals, and declarations after statements, is far from ergonomic in terms of compactness of the code; we cannot even use cleanup attributes to simplify the error handling and memory management, because those are not supported across all the toolchains we support. We're still visited by all the curses of developing in the '90s well into the third decade of the XXI century. Meanwhile, high level languages have higher order constructs and optimisations that make writing correct code a lot easier, and reliably performant.

:::

----

<p style="text-align:left;font-size:0.8em;">**Potter**: It damn well hurts!</p>
<p style="text-align:left;font-size:0.8em;">**Lawrence**: Certainly it hurts.</p>
<p style="text-align:left;font-size:0.8em;">**Potter**: Well, what's the trick, man?</p>
<p style="text-align:left;font-size:0.8em;">**Lawrence**: The trick, William Potter, *is not minding that it hurts*.</p>

::: notes

We can be real men, real women, or real furry creatures from Alpha Centauri, but at some point just not minding the pain isn't going to make the pain go away; and we cannot self-select our community based on pain tolerance—and yes, I'll admit this may seem rich coming from a Linux user of 25 years and a desktop Linux developer of 20. Is there an upper limit to the amount of pain a human being can sustain? Or a limit to the amount of pain human beings can inflict upon others? The existence of the C ISO working group seems to indicate that the answer to both questions is negative. The standard body for the C programming language will not save us. It's pointless to try, and it's pointless to ask the toolchain developers to help us.

:::

----

![](./images/c-does-not-protect-you.png)

::: notes

And if you don't believe me, this is straight from the ISO C working group.

:::

----

# Part II: How?

::: notes

How would you take a code base that measures in the half a million lines of C code range, and integrate a higher level language into it, in order to replace and augment that C code? The word you're looking for is: carefully. Luckily for us, other projects have done this already!

:::

----

## Case studies

::: notes

GTK would not be the first project looking at integrating code in multiple languages. Let's look at other projects that made this jump, and what kind of constraints they are operating under.

:::

----

### Case study: **librsvg**

::: notes

Librsvg is probably the most notable project that has been incrementally ported over from GObject/C to Rust. It still uses Autotools, but all it does is launching Cargo. Librsvg uses various crates, which are then vendored into release archives. The overall API surface is pretty small, so it's essentially a handwritten C header file with a bunch of C symbols; it's small enough that it doesn't even require code generation through c-bindgen, though it's theoretically possible to generate GObject/C code from the Rust API. Librsvg is also exposed as a native Rust crate, for Rust native code.

:::

----

### Case study: **GStreamer**

::: notes

GStreamer is The Other consumer of the GObject type system. As long as you implement a GObject, you can write pipeline elements in any language you want—though it seems that, as of this moment, Rust is the favourite. Out of tree elements can be written using the Rust bindings based on gobject-introspection, but GStreamer has recently started including Rust code in tree. The in-tree Rust is limited to a (security sensitive) binary with no dependencies outside the Rust standard library, built with Meson, but the future looks like more Rust and more dependencies on Rust crates through Meson subprojects.

:::

----

### Case study: **Mesa**

::: notes

Mesa started off as a C code base. It introduced C++ into it because, let's be honest, writing a parser and a compiler for GPU-oriented languages like GLSL and its intermediate representations is easier to do when you can crib from the Design Patterns and Dragon book examples; then C++ was phased out when that particular IR was replaced with something that didn't suck, and these days it only lives in a few drivers. Unperturbed by that particular misadventure, Mesa introduced Rust for implementing things like the OpenCL driver. The current limitations are: no Cargo, only Meson; no external crates, only standard library. The Mesa developers laid down most of the work inside Meson to ensure both the build of Rust internal libraries, as well as invoking the C header generator needed in order to call into Rust code from C and C++.

:::

----

### Case study: **Linux**

::: notes

Just like Mesa, the Linux kernel has integrated Rust into its own code base. Unlike Mesa, it has a project-wide set of internal API that allows exposing kernel objects from C to Rust and vice versa. Of course, since the Linux kernel has no access to the C standard library, there's also no access to the Rust standard library—except for the API that can be re-implemented on top of the kernel API. Rust is currently meant to be used to implement drivers, not core kernel functionality; but it's not out of the realm of possibility that, with more Rust knowledge getting introduced into the developers community, we could see core parts of the Linux kernel written in Rust.

:::

----

## Yes, I said Rust

::: notes

You may have noticed that all the case studies above have a thing in common: they are large C (and C++) code bases that integrated Rust into their toolchain, mainly as a way to introduce safety and high level language constructs where there was neither. If other people did it, we can take that as a template, or at least as a guide on what to do and what to avoid.

:::

----

## Really?

::: notes

Listen, I'm not going to try and sell Rust to you. C developers like small languages, and experienced C developers like mucking about with languages that let them poke at the lower layers of the OS. Nothing's going to change that, especially not a talk from me, a professional C developer that has been working on GNOME-related technology for both fun and salary for the past 20 years. What I can tell you, though, is that you can teach old dogs new tricks, and that there is a potential of getting more people involved without necessarily teaching them the monsters manual of Advanced Dungeons & Dragons as a precondition. Yes, you may not like the language; yes, you may find code bases written in Rust to look like an unfortunate accident in a glyphs factory; but the fact of the matter is that no language is going to look like C, especially no language worth using written in the XXI century. Don't bother trying.

:::

----

# Part III: Oxidising GTK

::: notes

Title drop!

:::

----

## The programmer motto

![](./images/hatsune-pinboard.png)

> We do these things not because they are easy, but because we thought they were going to be easy

::: notes

As creator of the Pinboard bookmark services—and world renowned vocaloid—Hatsune Miku said in her famous tweet, programmers put hard things off until it becomes untenable for them to continue to do so, and any movement towards change is the result of a dramatic underestimation of the effort involved. We've been doing that for decades, and the thesis of this entire presentation is that the window of time for tenability is closing.

:::

----

![](images/unsafe-blocks.png)

> from @jam1garner

::: notes

For any avoidance of doubt: GTK is not really a security boundary. Like: at all. You should never, ever run GTK code as root, or with elevated privileges. GTK itself will try really hard to discourage you from doing this. This doesn't mean that GTK's code should not take into account safety, especially if the compiler and the type system can help us developers, and mock us for our all too human shortcomings in the process. Safety in the context of GTK typically means minimising the ability to eat our users' data; it means also detecting a bunch of issues early, at compile time, instead of hunting them down at run time because somebody filed a bug somewhere. Encoding the ability to say that a block of memory has moved ownership between parts of the code into types themselves under the control of the compiler, is much more powerful than using specially crafted annotations in our documentation comments and a C parser written in Python, flex, and bison.

:::

----

## ABI boundary conditions

::: notes

GTK has two ABI boundaries: outwards, and inwards. The former is the ABI we expose to our users, and that other libraries and applications consume; this ABI is guaranteed to be stable, and consumable through anything that can deal with a C ABI, mostly through FFI. The latter is the ABI we use internally to GTK: the set of symbols that GTK can access in order to implement itself. The internal ABI is not stable, for obvious reasons, but it tends to be documented and consistent. It is based on shared patterns and other libraries, but it is meant to be wrapped by public symbols. This means we cannot use the same code generation mechanisms that the outward boundary can use—like introspection and existing bindings; on the other hand, we can make it as ergonomic as we want, to fit in with our requirements, instead of finding something that will be idiomatic for application developers.

:::

----

![](./images/rdw-capi.png)

::: notes

We can expose symbols through a C ABI straight from the Rust code, and once we build a static library with them, they do work precisely like native C symbols; of course, the C boundary gets messy, and currently it can only be hand-written. The example here comes from a Rust crate that also exposes a C ABI, similar to librsvg.

:::

----

![](./images/meson-rusticl.png)

::: notes

On the other side of the ABI boundary, we would need a lot more in order to deal with things like generating Rust code from C symbols; essentially, we'd be running our own small internal Rust bindings. These, too, would be handwritten because we cannot rely on introspection: we are building a dependency of the public GTK API, so we can't run the introspection scanner until we have a library; and anyway, we don't have introspection data for internal API. Luckily for us, the GLib bindings for Rust have a substantial amount of handwritten code to simplify moving objects across the language boundary, and with some work we can include a smaller version of that particular API surface inside GTK itself.

:::

----

## (Cargo) Cult Deprogramming

::: notes

The real issue is dealing with Rust dependencies if we don't want to copy-paste or hand-roll a bunch of code inside GTK itself. Even if I manage to sell Rust to a few C developers, selling the whole Crates ecosystem to distributors for a *very* core system library isn't going to work—at least, not yet. Turns out that distributions borne out of shipping a ton of C code are not great at incorporating non-C code in their tools and processes. Took Linux distributions *years* to be able to cope with C++, and C++ doesn't have a vast ecosystem of easily accessible libraries to begin with. Linux distributions are C-oriented platforms, and the people making them are sysadmins. We're trying to appeal to new developers, but you have to remember that GTK is both an application and a system level library: we cannot Flatpak the whole session.

:::

----

## Cargo is (not) good

::: notes

Let's also address the fact that Cargo is not a great tool. It's clearly the result of an experiment that ran for too long, and now we are fully into sunk cost fallacy mode. As a dependency manager is acceptable, but as a build system is ridiculously bad. The only good thing it brings to the table is the declarative format—though even that it is blemished by the escape hatch of the `build.rs` file—because nothing says "good build system" like "write native code to build a binary to generate lines of text that will be parsed at configuration time in order to decide what to build, and how to build it".

:::

----

## Do you have time to talk about our lord and saviour Meson?

::: notes

Building software ought to be boring, and yet is likely the main source of frustration in existence today. Taking care of portability, dependencies, code generation, and doing so reliably, is painful. Anything that moves the ownership of that pain from maintainers to something, or someone, else is considered an improvement. That's the main reason why the GNOME project moved from Autotools, one of the pinnacles of self-inflicted pain in the whole history of computing, to Meson in what I'd consider record time. Thanks to the work of GStreamer and Mesa developers, we might get better integration between Meson and Rust soon, with the ability to manage crates as subprojects in a way that feels idiomatic. Of course, subprojects in Meson have been a somewhat constant source of strife for the GTK maintainers, especially when it comes to CI, so it's abundantly clear that there cannot be free lunches.

:::

----

![](./images/meson-gtk-oxide.png)

::: notes

At the end of the day, adding Rust as a static library isn't much more complicated than this—but that's just an illusion of functionality. There's a lot more that needs to happen, and the interface between all these bits is still pretty rough. It would require commitment not just from the GTK developers, but also from build system maintainers and downstream packagers.

:::

----

## Introspection (of sorts)

::: notes

The main selling point of the GNOME platform is not our scintillating personality, or the fact that we have achieved in 25 years and with a box of scraps in a cave what trillion dollars companies did with tens of thousands of employees. The main selling point is that we provide a neutral platform that can be consumed from various programming languages. There are, of course, major downsides to that particular choice, first and foremost that the only way to achieve that is by providing a C ABI. For that, we have introspection, which is a machine-readable description of that C ABI, and a bunch of stuff on top. This means that any effort of replacing C needs to generate a C ABI and, possibly, the introspection data for it. We don't have the latter, yet, but we can use cbindgen to generate C headers, and a modified cbindgen to generate GObject stubs from Rust. Nevertheless, it's not at all frictionless, and requires a lot more work.

:::

----

## Oxidising from the roo(t|f)

::: notes

Moving the consumers of our platforms to better languages is definitely going to have a measurable impact on the overall quality of the platform itself. Writing applications in safer, higher level languages, would reduce the issues we find inside applications themselves. Do not waste everybody's time (including yours) porting applications from JavaScript to Vala, or from Python to Rust; spend time writing new, and better applications, instead. If you really want to port unmaintained applications to your higher level language of choice, port them *away from C*. At the same time, lower level libraries should start integrating code written in higher level languages. Librsvg and GStreamer are two good examples of how it can be done. Components that sit at a security boundary should also be vetted for safety, and spending time on implementing replacements or ports to safer languages while still exposing the same D-Bus or C interfaces should take priority over the next music player. Maybe we can reduce the size of C code in GTK simply by having fewer components there, and more libraries in higher level languages.

:::

----

## Making GObject/C suck less

::: notes

GObject/C is a pain to write and to test, but we can still make it less of a time sink. Consistent use of newer C standards makes code bases a bit better, assuming that our supported toolchains provide those facilities. We should add more functionality to GObject and introspection, so that anything built on top can operate more reliably. Every API needs to conform to shared patterns, so we don't have to add heuristics and escape hatches everywhere just because some maintainer feels like their project is its own specialest snowflake that ever existed. Ideally, translating the guarantees that the compiler can give us into an interoperable C ABI and a description of that ABI for other languages to consume, would go a long way into making GObject safer and more reliable.

:::

----

# Part IV: What ifs and could've beens

::: notes

Rust is not the only option for moving GTK towards a smaller C footprint, of course. Just the one that looks more likely to me. There are other options, though, and somebody else might find them more palatable or more interesting.

:::

----

## C++

::: notes

C++ is a kitchen sink trash compactor, so maybe we can find something into it that can improve they way we write GTK, possible without losing our hands in the process. For instance: Harfbuzz is written in a *very* strict subset of C++, mainly just plain old data types, strict templates, no exceptions, and no C++ standard library. VTE went full C++, but still exposes a GObject/C ABI for people to consume. Maybe there's something in between from which we can take inspiration. The main downside of C++ is that you **cannot** mix C and C++ without paying a price, so you will inevitably end up porting all your C code to C++.

:::

----

## Carbon

::: notes

Carbon is an attempt at placing restrictions on C++ in such a way that you can consider slowly porting a C++ code base over to it. It comes with its own LLVM, and it's a Google experiment, and we all know how they end up; but Rust was a Mozilla experiment and it kinda turned out fine, so maybe there's a tiny glimmer of hope.

Of course I'm kidding.

:::

----

## Zig

::: notes

Zig is interoperable with C to the point that you can use the Zig compiler to compile C and C++; but like basically every low level language these days, it comes with its own LLVM, with little integration with other projects, not much documentation, and a language that isn't fully baked, yet.  It *could* be interesting in the future, though. Zig is probably familiar to C developers in terms of memory model, but it gets firmly into the uncanny valley when it comes to syntax.

:::

----

## DIY languages

::: notes

Apple wrote Swing to replace Objective C, another ad hoc language that they adopted to improve their (mainly C) platform. Maybe we should try Vala again, possibly with a proper formal specification, this time. Investing in a programming language is probably even more complicated than having people investing in a GUI toolkit, but stranger things have happened. The main limitations of Vala is that it cannot fix GObject and its type system, which precludes us from being actually compatible with multiple languages; so making Vala better implies fixing GObject, which implies breaking everything.

:::

----

## ὕβρις (húbris) and ἁμαρτία (hamartía)

**húbris**: Excessive pride, presumption, or arrogance (originally toward the gods)

**hamartía**: The tragic flaw of the protagonist in a literary tragedy.

::: notes

What about starting a toolkit from scratch and sunsetting GTK? It's just software, after all; and if there's one thing programmers have in spades, it is hubris. Of course, the cost is not trivial: I say that as the former maintainer of Clutter. You still need to interface yourself with a base layer that is a bunch of C API calls; you need to deal with memory management, and with windowing system protocols, and IPC mechanisms. You need to settle on an API, and assuming you don't have the pull or the ability to dictate a single programming language, you will need to have a way to expose a C ABI and its description so that other programming languages can consume it. After that, you have to implement all the bits and pieces necessary to take over a desktop: accessibility, drag and drop, clipboards, printing, file selection, sandboxing. Then you'll have to ensure that application developers will use your new platform, which means documentation, tutorials, testing, portability, deployment, distribution. You'll need to do all of this with volunteers as well, because until you have something that can work, companies, especially the ones that promise support to their clients in exchange for a fair chunk of money, will not touch this new project with a ten foot barge pole.

:::

----

## Giving up

::: notes

There's always this option. Keep working on GLib and GTK; add new features, update to new versions of the C standard, deprecate old API, do a new major release. Rinse. Repeat. Keep trying to get new contributors. It's not like most of us will ever be able to retire and see a state pension, so maybe we'll get to work on GTK forever.

:::

----

# Experiments are here for a reason

::: notes

The important message I'd like to convey is that if you want to experiment, you really should. Now it's the time. Prove me wrong. Prove me right. Take heed of what I found, or go your own way. Convince other people I'm wrong and you are right. Or work with us, and figure out how to steer GTK towards a different future.

:::

----

# Thank you

----

![](./images/vimes-censored.jpg)

----

# Q & A
